import { createApp } from 'vue'
import App from './App.vue'
import bootstrap from 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import router from './router'

const app  = createApp(App).use(router);
app.use(bootstrap);
app.mount('#app');
